# ChatbotDemo



## Getting started

The main objective of this project is to offer the user semantically related extra information to a specific answer previously obtain (also automatically) from a question answering (QA) system. To achieve the objective, this project presents a semantic storytelling approach that is composed of a QA module and a recommender system module. 

## QA module

The QA module is an adapted version of an existing system that worked in English to be able to work with documents in German. To run it, you have to download the Haystack code (https://gitlab.com/speaker-projekt/questionanswering/haystack-qa-service) and follow the steps described there.

## Recommender system module

The goal of this module is to identify and to suggest new content for the user that could be semantically related to the question and the answer. To accomplish this goal, the module is composed of an offline processing and an online processing.

### Offline processing

In the offline processing, we determine the semantic relations that can exist between documents of the dataset and storing them (the documents and the relations) in a database for their use in the online processing.  To get the relations, you have to run the relationPredictor script. For that we create a service based on this repository https://github.com/DFKI-NLP/semantic-storytelling and we use as a dataset the origin column of the data.csv file stored in the data folder. As output, you get two files (Document.txt and Relation.txt) that are also stored in the same folder.

### Online processing

The online processing allows us to look for the semantic relations through the information that we have obtained from the offline processing and the QA response. 

## Graphical user interface

To run the GUI, apart from running the QA module, we also need to run the DMApplication, taking into account that you have to crear an account in the European Language Grid (https://www.european-language-grid.eu) in order to get the token that allows you to use the ASR system, in this case, the Hensoldt Analytics Speech-to-text for English (https://live.european-language-grid.eu/catalogue/tool-service/20891/overview/) system. This token has to be changed from time to time. The last step is to open the chatbot in a Webbrowser
