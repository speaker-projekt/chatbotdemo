import service
import json
import os
import csv

def search_files_txt(path): 
	text_files = [] 
	files = os.listdir(path) 
	for file in files: 
		if file[-4:] == '.txt': 
			text_files.append(file) 
	return text_files 

def search_files_csv(path): 
	text_files = [] 
	files = os.listdir(path) 
	for file in files: 
		
		if file[-4:] == '.csv': 
			text_files.append(file)
			
	return text_files 

def split_paragrahp_txt(text_files):
    i = 0
    f_write = open("Document.txt","w")
    
    for file in text_files:
        with open(file, "r") as f:
            line = f.readline()
            while line:
                print(line)
                if line.strip() !='':
                    i+=1
                    f_write.write(line.strip() + ";" + str(i)+"\n")  
                line = f.readline()
 
        f.close()
    f_write.close()
    
def split_paragrahp_csv(text_files):
    i = 0
    f_write = open("Document.txt","w")
   
    for file in text_files:
        with open(file, newline='', mode='r', encoding='utf-8') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            next(csvfile)
            for row in spamreader:
                i+=1
                f_write.write(row[0] + ";" + str(i)+"\n")
        csvfile.close()
    f_write.close()
    
    
  
def semantic_relation(file):
   
    with open(file, "r") as f:
        line = f.readline()
        dictDoc = dict()
        while line:
            index = line[line.rindex(';')+1:len(line)-1]  
            text = line[0:line.rindex(';')-1]
            dictDoc.update({index:text})
            line = f.readline()
    f.close()
    
    dictRel = dict()
    dictAux = dict()
    
    
    
    
    for i in dictDoc.keys():
        dictAux = dict()
        for j in dictDoc.keys():
            if not i==j:
                dict_result = service.prediction_output(dictDoc[i],dictDoc[j])
                dictSub = dict()
                for l in range(len(dict_result)):
                    
                    for key,value in dict_result[l].items():
                        
                        if value >= 0.01:
                            dictSub.update({key:value})
                                      
                
                dictAux.update({j:dictSub})
        dictRel.update({i:dictAux})
    
   
    
    print(dictRel)
    
    f_write = open("Relation.txt","w")
    
    f_write.write(json.dumps(dictRel))
    
    f_write.close()
    

def main():
    
    text_files = ["data.csv"] 
    print(text_files)
    if text_files[-4:] == '.txt':
        split_paragrahp_txt(text_files)
    else:
        split_paragrahp_csv(text_files)
    semantic_relation("Document.txt")
   
   
if __name__ == "__main__":
    main()
