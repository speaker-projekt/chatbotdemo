from transformers import BertTokenizerFast, DebertaTokenizerFast
from transformers import AutoModelForSequenceClassification, TrainingArguments, Trainer
import torch
import numpy as np
import random
import pandas as pd
from IPython.display import display, HTML
import json

labels = ["none", "attribution", "causal", "conditional", "contrast", "description", "equivalence", "fulfillment", "identity", "purpose", "summary", "temporal"]
base_model = "microsoft-deberta-base"
num_epoch = 10
input_strategy = 2
subset_labels = False
# Which exact fold to use
fold = 4
data_path = "data/export"
model_path = "data/model"
model_checkpoint = f"{model_path}/"

from sklearn.metrics import classification_report
import collections


softmax = torch.nn.Softmax(dim=-1)

def flatten(d, parent_key='', sep='__'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.abc.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)

def compute_metrics(eval_pred):
    global labels
    predictions, true_labels = eval_pred
    # take most probable guess
    predictions = np.argmax(predictions, axis=-1)
    return flatten(classification_report(
        y_true=true_labels,
        y_pred=predictions,
        zero_division=0,
        output_dict=True))

# Tokenizer
if "microsoft" in base_model:
    tokenizer = DebertaTokenizerFast.from_pretrained(model_checkpoint)
else:
    tokenizer = BertTokenizerFast.from_pretrained(model_checkpoint)

num_labels = len(labels)
model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint, num_labels=num_labels)
trainer = Trainer(
    model,
    tokenizer=tokenizer,
    compute_metrics=compute_metrics
)

class SemanticDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)
    
def prediction_output(origin, target):
    test_encodings = tokenizer([origin], [target], truncation=True, padding=True, return_token_type_ids=True)
    test_dataset = SemanticDataset(test_encodings, [0])
    prediction_output = trainer.predict(test_dataset)    
    label_data = []
    for n in range(len(prediction_output.label_ids)):
        i = np.argmax(prediction_output.predictions[n], axis=-1)
        label_data.append([labels[i]])
    df_pred_labels = pd.DataFrame(label_data, columns=["Prediction"])
    # Prediction Metrics
    pred = softmax(torch.from_numpy(prediction_output.predictions))
    df_predictions = pd.DataFrame(pred.numpy(), columns=labels)
    json_list = json.loads(json.dumps(list(df_predictions.T.to_dict().values())))
    return json_list
       

# driver function
if __name__ == '__main__':
    prediction_output("Sentence 1 is better than anything.","After that, we just enjoyed the situation.")
    
