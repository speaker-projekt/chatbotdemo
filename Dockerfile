FROM openkbs/jdk11-mvn-py3:latest

MAINTAINER Julian Moreno Schneider <julian.moreno_schneider@dfki.de>

RUN sudo apt-get update 
RUN sudo apt-get install -y python3-pip 

RUN sudo mkdir /var/maven/ && sudo chmod -R 777 /var/maven 
RUN sudo mkdir /tmp/chat/ && sudo chmod -R 777 /tmp/chat
ENV MAVEN_CONFIG /var/maven/.m2

ADD pom.xml /tmp/chat
RUN cd /tmp/chat && mvn -B -e -C -T 1C -Duser.home=/var/maven org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline 

RUN sudo chmod -R 777 /var/maven


ADD . /tmp/chat

WORKDIR /tmp/chat
RUN mvn -Duser.home=/var/maven clean package -DskipTests

RUN sudo chmod -R 777 /tmp/chat
EXPOSE 8080
CMD mvn -Duser.home=/var/maven spring-boot:run -Dspring-boot.run.jvmArguments="-Xms2048m -Xmx4096m"

