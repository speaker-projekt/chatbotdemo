package de.dfki.speaker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;

import java.net.HttpURLConnection;

import java.net.URL;
import java.net.URLEncoder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import java.util.List;
import java.util.Map;
import java.util.Random;


import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;


@Component
public class Engine {
	List<String> greetings = Arrays.asList("hello","hi", "hey");
	List<String> assertion = Arrays.asList("yes", "yes, please", "of course");
	List<String> goodbye = Arrays.asList("bye", "good bye", "see you later");
	List<String> thanks = Arrays.asList("thanks", "thank you", "that's helpful", "awesome, thanks", "thanks for helping me", "great", "no, thanks", "no thanks");

	public String recognizeIntent(String query) {

		System.out.println("query: " + query);                                  
		if(greetings.contains(query.toLowerCase())) {
			return "greetings";
		}
		else if(goodbye.contains(query.toLowerCase())) {
			return "goodbye";
		}
		else if(assertion.contains(query.toLowerCase())) {
			return "assertion";
		}
		else if(thanks.contains(query.toLowerCase())) {
			return "thanks";
		}
		else {
			return "question";
		}

	}

	public String getAnswer(String query, String intent) throws Exception {
		List<String> pattern;
		Random r = new Random();
		
		if(intent.equals("greetings")) {
			pattern = Arrays.asList("Hello","Hi", "Welcome");
			int num = r.nextInt(pattern.size());
			return pattern.get(num);
		}
		else if(intent.equals("goodbye")) {
			pattern = Arrays.asList("Have a nice time","Bye", "See you soon");
			int num = r.nextInt(pattern.size());
			return pattern.get(num);
		}
		else if(intent.equals("assertion")) {
			pattern = Arrays.asList("What can I help you with?", "What is your question?");
			int num = r.nextInt(pattern.size());
			return pattern.get(num);
		}
		else if(intent.equals("thanks")) {
			pattern = Arrays.asList("Happy to help!", "Any time","You're welcome", "My pleasure");
			int num = r.nextInt(pattern.size());
			return pattern.get(num);
		}
		else if (intent.equals("question")){
			return getQA(query);
		}
		else {
			return "Sorry I cannot understand you. Could you reformulate your text?";
		}

	}

	public String getQA(String query) throws Exception {

		//URL url = new URL("http://localhost:5000/get_answer");
		URL url = new URL("http://haystack:5000/get_answer");
		Map<String, Object> params = new LinkedHashMap<>();

		params.put("body", query);
		System.out.println("hola");

		StringBuilder postData = new StringBuilder();
		for (Map.Entry<String, Object> param : params.entrySet()) {
			if (postData.length() != 0)
				postData.append('&');
			postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
			postData.append('=');
			postData.append(URLEncoder.encode(String.valueOf(param.getValue()),
					"UTF-8"));
		}
		byte[] postDataBytes = postData.toString().getBytes("UTF-8");

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type",
				"application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length",
				String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);

		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		for (int c = in.read(); c != -1; c = in.read())
			sb.append((char)c);

		String answer = sb.toString();

		return answer;
	}

	public JSONObject getExtraInformationForAnswer(String query) throws Exception {
		
		JSONObject extra = new JSONObject();
		
		try {

			File docs = new File ("data/Document.txt");
			BufferedReader br = new BufferedReader(new FileReader(docs));

			
			String line="";
			int pos = -1;
			String text = "";
			String numKey = "";
			String index = "";
			HashMap <String, String> docMap = new HashMap<String, String>();
			while((line=br.readLine())!=null) {

				pos = line.lastIndexOf(';');
				text = line.substring(0, pos);
				numKey = line.substring(pos+1);

				docMap.put(numKey, text);
			}
	
			for (String i : docMap.keySet())  {	 
				
				if(docMap.get(i).contains(query)) {
					index = i;	
					System.out.println("Index:" + index);
					break;
				}
			}
			File rel = new File ("data/Relation.txt");
			
			String content = FileUtils.readFileToString(rel,"utf-8");
			
			JSONObject jsonObject = new JSONObject(content);
			JSONObject objRelations = (JSONObject) jsonObject.get(index);	
			System.out.println("Index "+ index + " Content " + objRelations);
			
			for (String i : objRelations.keySet())  {
				JSONObject docRel = new JSONObject();
				docRel.append("id", i);
				docRel.append("text", docMap.get(i));
				JSONObject r = objRelations.getJSONObject(i);
				docRel.append("relations", r);
				extra.append("document", docRel);
			}

			
			System.out.println("Extra " + extra);

		}
		catch(Exception e){
			e.printStackTrace();
		}

		return extra;

	}

}
