package de.dfki.speaker;


import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RawBody;
import com.mashape.unirest.request.HttpRequest;


import io.swagger.v3.oas.annotations.Operation;


@RestController
@RequestMapping("dialogmanager")
@SpringBootApplication(exclude = {
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class RestAPIController {

	@Autowired
	Engine engine;

	@PostConstruct
	public void setup() {
		try {
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
	}

	@Operation(hidden=true)
	@RequestMapping(value = "/testURL", method = { RequestMethod.POST, RequestMethod.GET })
	public ResponseEntity<String> testURL(
			@RequestBody(required = false) String postBody) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain");
		ResponseEntity<String> response = new ResponseEntity<String>("The DialogManager restcontroller is working properly", responseHeaders, HttpStatus.OK);
		return response;
	}

	@CrossOrigin
	//		@RequestMapping(value = "/segmentDocument",
	//		method = {	RequestMethod.POST },
	//		consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE},
	//		produces = {MediaType.APPLICATION_JSON_VALUE}
	//				)
	@RequestMapping(value = "/dialog",method = {RequestMethod.POST})
	public ResponseEntity<String> dialog(
			HttpServletRequest request,
			@RequestHeader(value = "Accept", required = false) String acceptHeader,
			@RequestHeader(value = "Content-Type", required = false) String contentTypeHeader,
			@RequestParam Map<String, String> allParams,
			@RequestParam(value = "query",required = false) String query,
			@RequestBody(required = false) String postBody) throws Exception {
		String content = "";
		if(query==null && postBody==null){
			String msg = "Query or body cannot be NULL or empty.";
			System.out.println(msg);
			throw new Exception(msg);
		}
		else {
			
			if(query!= null && postBody==null){
				content = query;
			}
			else {
				
				content = postBody;
			}
		}
			
		
		

		

		HttpHeaders responseHeaders = new HttpHeaders();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		try {
			responseHeaders.add("Content-Type", "text/plain");
			
			
			
			
			String intent = engine.recognizeIntent(content);
			System.out.println(intent);
			System.out.println("content " + content);
			String answer = engine.getAnswer(content,intent);
			
			
			boolean rightDone = true;
			
			String body = "ERROR, the DM cannot process the request.";
			if(rightDone) {
				responseHeaders.add("Content-Type", "application/json");
				
				JSONObject subtext = new JSONObject();
				
				JSONObject features = new JSONObject();
				features.put("intent", intent);
				features.put("answer", answer);
				
				if(intent.equalsIgnoreCase("question")) {
					answer = answer.substring(1, answer.length()-1);
					JSONObject jobject = new JSONObject(answer);
					JSONObject infosRelation = engine.getExtraInformationForAnswer((String)jobject.get("context"));
					features.put("extraInformation", infosRelation);
				}
				else {
					features.put("extraInformation", "");
				}
				
				subtext.put("content", content);
				subtext.put("features", features);
				
				JSONObject text = new JSONObject();
				text.put("type", "texts");
				text.put("texts", subtext);
				
				
				
				
				JSONObject response = new JSONObject();
				response.put("response", text);
				body = response.toString();
				status = HttpStatus.OK;
			}
			ResponseEntity<String> response = new ResponseEntity<String>(body, responseHeaders, status);
			return response;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
	}
	
	@Operation(hidden=true)
    @RequestMapping(value = "/elgASR", method = { RequestMethod.POST, RequestMethod.GET })
    public ResponseEntity<String> elgASR(
               @RequestParam(value="data") MultipartFile file,
               @RequestParam(value="fname") String fname,
            @RequestBody(required = false) String postBody) throws Exception {
        Object body = file.getBytes();
        String endpoint = "https://live.european-language-grid.eu/execution/process/hens-asr-en";
        HttpRequest request;
        RawBody rb = null;
        if(body instanceof String) {
            rb = Unirest.post(endpoint).body(((String) body).getBytes());
        }
        else if(body instanceof byte[]) {
            rb = Unirest.post(endpoint).body((byte[])body);
        }
        request = rb.getHttpRequest();
        request = request.header("Content-Type", "audio/mpeg");
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ0MVhwNXlCb1VpREM0emxOdTcyeUNwT3hKRG1fQTdObVRkSjZRcVRiQW5nIn0.eyJleHAiOjE2NzQwNzEyMjYsImlhdCI6MTY3NDA2MDQyNiwiYXV0aF90aW1lIjoxNjc0MDYwNDI1LCJqdGkiOiJiYTkwMjFjMi05N2U1LTQ2ZjMtOTg2Mi1hOTA1NWNhMjU1N2YiLCJpc3MiOiJodHRwczovL2xpdmUuZXVyb3BlYW4tbGFuZ3VhZ2UtZ3JpZC5ldS9hdXRoL3JlYWxtcy9FTEciLCJhdWQiOiJlbGdfZ2F0ZWtlZXBlciIsInN1YiI6ImE1N2U3NGI3LTVjNGEtNGJjOC04NmI1LWEyMWJmNDIzZTkxYyIsInR5cCI6IkJlYXJlciIsImF6cCI6InJlYWN0LWNsaWVudCIsIm5vbmNlIjoiMDVjNzdmZGQtOWU1NC00M2UxLWFjMGMtNWEzZDY5NmVhZTM2Iiwic2Vzc2lvbl9zdGF0ZSI6IjUyZWMxMTRlLTUxYTgtNDhkOC1iMmY2LTI0ZDU0ZGQ5N2FlNiIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cHM6Ly9saXZlLmV1cm9wZWFuLWxhbmd1YWdlLWdyaWQuZXUvIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJwcm92aWRlciIsIkVMRy1hZG1pbiIsImFkbWluIiwiY29uc3VtZXIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFjdC1jbGllbnQiOnsicm9sZXMiOlsiY29uc3VtZXIiXX19LCJzY29wZSI6Im9wZW5pZCBFTEctcHJvZmlsZSBwcm9maWxlIGVtYWlsIiwic2lkIjoiNTJlYzExNGUtNTFhOC00OGQ4LWIyZjYtMjRkNTRkZDk3YWU2IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInJvbGVzIjpbInByb3ZpZGVyIiwiRUxHLWFkbWluIiwiYWRtaW4iLCJjb25zdW1lciIsInByb3ZpZGVyIiwiRUxHLWFkbWluIiwiYWRtaW4iLCJjb25zdW1lciJdLCJuYW1lIjoiSnVsaWFuIE1vcmVubyBTY2huZWlkZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJqdWxpYW4ubW9yZW5vX3NjaG5laWRlckBkZmtpLmRlIiwiZ2l2ZW5fbmFtZSI6Ikp1bGlhbiIsImZhbWlseV9uYW1lIjoiTW9yZW5vIFNjaG5laWRlciIsImVtYWlsIjoianVsaWFuLm1vcmVub19zY2huZWlkZXJAZGZraS5kZSJ9.YaexEGu8r5qTyTNzeHvNK2srJRz0LEn8w9OQCuvwSaVmD0Qfp91p06qIxt02dhzaiK62Axz947yVWpiPOfhs5zmqIMuaoVzbdNuX8WUvMBMu81QeMbvUQrNpfCmGuP7pBQ6JUpIjEfNkuCqWUUK0MGve-cIfxOtrSa-Demwk8ZwLc4AR6ReijWEPXQ0n0K1z5Dm5i-KK5pdC7XIgTHPWWXNi6xWtyoa-CNQUM7je0l42um5Dg5jLsSPz07ZBW7r4vyd5sxiVY-j8TjhJIcXDm3XvBxHhueoHTU2hGHzycbIpND7UlZxnlcVXWHn4bj2FRJjTdaDWP42wBdMGA7SBvA";
        request = request.header("Authorization", "Bearer "+token);
        System.out.println("DEBUG: WE are requesting: "+request.toString());
        System.out.println("DEBUG: WE are requesting: "+request.getBody());     
        HttpResponse<String> response371 = request.asString();
        JSONObject jsonResponse = null;
        if(response371.getStatus()==200) {
            jsonResponse = new JSONObject(response371.getBody());           
            String responseText = "";
            if(jsonResponse.has("response") &&
                    jsonResponse.getJSONObject("response").has("texts")) {
                JSONArray array = jsonResponse.getJSONObject("response").getJSONArray("texts");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject json1 = array.getJSONObject(i);
                    responseText = responseText + " " + json1.getString("content");
                }
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
            ResponseEntity<String> response = new ResponseEntity<String>(responseText, responseHeaders, HttpStatus.OK);
            return response;
        }
        else {
            System.out.println("ERROR: "+response371.getStatus());
            System.out.println("ERROR: "+response371.getBody());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("Content-Type", "text/plain");
            ResponseEntity<String> response = new ResponseEntity<String>(response371.getBody(), responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
            return response;
        }
    }
}
