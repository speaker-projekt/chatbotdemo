package de.dfki.speaker;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class WebController {

	
	String footerText = "&copy 2022 Speaker Project. All Rights Reserved";
	String logohtml = "<img src=\"./DFKI_SPEAKER_Logo.png\" alt=\"Speaker Logo\" width=\"70\" height=\"70\">";
	String footerLogo = "<img src=\"./DFKI_SPEAKER_Logo.png\" alt=\"Speaker Logo\" width=\"75\">";

	public String getMenuText() {

		String s = "";
		s = s + "<a class=\"p-2 link-secondary\" href=\"./kse.html\">Knowledge Storage Ecosystem</a>" + "\n";
		s = s + "<a class=\"p-2 link-secondary\" href=\"./ke.html\">Knowledge Extraction</a>" + "\n";
		s = s + "<a class=\"p-2 link-secondary\" href=\"./qa.html\">Q&A</a>" + "\n";
		s = s + "<a class=\"p-2 link-secondary\" href=\"./chatbot.html\">Chatbot</a>" + "\n";
		s = s + "<a class=\"p-2 link-secondary\" href=\"./recommender.html\">Recommender System</a>" + "\n";

		return s;

	}


     /**
     * @return The index website of the GUI for the Workflow Manager
     */

    @GetMapping("/demo.html")
    public String kse(org.springframework.ui.Model model) {

    	model.addAttribute("footertext", footerText);
    	String s = getMenuText();
    	System.out.println("Menu_Text:" + s);
    	model.addAttribute("menutext", s);
    	
        return "demo";

    }
    /**

     * @return 

     */

    @GetMapping("/chatbot.html")

    public String chatbot(org.springframework.ui.Model model) {

        model.addAttribute("footertext", footerText);
        model.addAttribute("logohtml", logohtml);
        model.addAttribute("footerLogo", footerLogo);
        String s = getMenuText();

        model.addAttribute("menutext", s);

        return "chatbot";

    }

    @RequestMapping("/")
    public String greeting(org.springframework.ui.Model model) {
        return "";
    }

}

