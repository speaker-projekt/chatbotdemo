package de.dfki.speaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.dfki.speaker"})
public class DMApplication {

	public static void main(String[] args) {
		SpringApplication.run(DMApplication.class, args);
	}

}
